type NoteName = 'c' | 'cs' | 'd' | 'ds' | 'e' | 'f' | 'fs' | 'g' | 'gs' | 'a' | 'as' | 'b'

enum NoteValue {
  whole = 1,
  half = 1/2,
  quarter = 1 / 4,
  eighth = 1 / 8,
  sixteenth = 1 / 16,
}

type Timing = 'straight' | 'swing';

enum Octave {
  subsubcontra = -1,
  subcontra = 0,
  contra = 1,
  great = 2,
  small = 3,
  onelined = 4,
  twolined = 5,
  threelined = 6,
  fourlined = 7,
  fivelined = 8,
  sixlined = 9,
}

type Rest = 'r';

interface SongDefinition {
  readonly tempo: number;
  readonly resolution: NoteValue;
  readonly timing: Timing,
  readonly tracks: TrackDefinition[],
  readonly drums: DrumTrackDefinition,
}

interface TrackDefinition {
  readonly wave: OscillatorType,
  readonly notes: NoteDefintion[],
  readonly mute?: boolean,
}


interface NoteDefintion {
  readonly 0: NoteTypeDefinition;
  readonly 1: NoteValue;
}

type NoteTypeDefinition = Rest | NoteKeyDefinition;

interface NoteKeyDefinition {
  readonly 0: NoteName,
  readonly 1: Octave,
}

type DrumTrackDefinition = DrumKickDefinition[];

type DrumKickDefinition = 'h' | 's' | 0;
