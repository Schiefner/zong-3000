import Note from './Note';
import Oscillator from './Oscillator';
import Song from './Song';

export default class Track {
  song: Song;
  oscillator: Oscillator;
  mute: boolean;
  notesDefinition: NoteDefintion[];
  constructor(song: Song, track: TrackDefinition) {
    this.song = song;
    this.oscillator = new Oscillator(song.context, track.wave);
    this.mute = track.mute || false;
    this.notesDefinition = track.notes;
  }

  get notes() {
    const notes = this.notesDefinition.reduce(
      (notes: (Note | null)[], note) => {
        const newNote = new Note(this, note);
        const nulls = Array(note[1] / this.song.resolution - 1).map(() => null);
        return [...notes, newNote, ...nulls];
      },
      []
    )
    notes.forEach((note: Note | null, index: number) => {
      if (note) {
        note.firstOfPair = !!(index % 2);
      }
    });
    return notes;
  }

  hasNote(index: number) {
    return index < this.notes.length;
  }

  playNote(index: number) {
    if (!this.mute) {
      const note = this.notes[index];
      if (note) {
        note.play();
      }
    }
  }
}
