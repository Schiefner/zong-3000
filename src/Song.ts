import Track from './Track';
import DrumTrack from './DrumTrack';
import { calculateDuration } from './utils';

export default class Song {
  context: AudioContext;
  tempo: number;
  timing: Timing;
  resolution: number;
  tracks: Track[];
  drumTrack: DrumTrack;
  index: number;
  playNextTimeout: number | undefined;
  constructor(song: SongDefinition, context: AudioContext) {
    this.context = context;
    this.tempo = song.tempo;
    this.timing = song.timing || 'straight';
    this.resolution = song.resolution;
    this.tracks = song.tracks.map(track => new Track(this, track));
    this.drumTrack = new DrumTrack(this, song.drums);
    this.index = 0;
  }

  play() {
    this.playNext();
  }

  playNext() {
    if (this.tracks.some(track => track.hasNote(this.index))) {
      this.tracks.forEach(t => t.playNote(this.index));
      this.drumTrack.playKick(this.index);
      this.index++;
      this.playNextTimeout = setTimeout(this.playNext.bind(this), this.tickMilliseconds());
    } else {
      this.stop();
    }
  }

  stop() {
    this.pause();
    this.index = 0;
  }

  pause() {
    clearTimeout(this.playNextTimeout);
    this.playNextTimeout = undefined;
  }

  isPlaying() {
    return !!this.playNextTimeout
  }

  tickMilliseconds() {
    return calculateDuration(
      this.resolution,
      this.tempo,
      this.resolution,
      this.timing,
      !!(this.index % 2),
    );
  }
}
